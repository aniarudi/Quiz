package pl.o2.quiz.mobile.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import pl.o2.quiz.mobile.data.QuizContract.*;
import pl.o2.quiz.mobile.data.QuizContract.Quiz;
import pl.o2.quiz.mobile.gson.*;

/**
 * Created by rudi on 08.04.18.
 */

public class QuizDBHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME = "quiz.db";

    private static final int DATABASE_VERSION = 1;

    public QuizDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        /*
         * This String will contain a simple SQL statement that will create a table that will
         * cache our weather data.
         */
        final String SQL_CREATE_QUIZ_TABLE =

                "CREATE TABLE " + Quiz.TABLE_NAME + " (" +

                        Quiz.COLUMN_QUIZ_ID             + " INTEGER PRIMARY KEY, " +
                        Quiz.COLUMN_QUIZ_TITLE          + " TEXT, "                +

                        Quiz.COLUMN_QUIZ_PHOTO + " TEXT, "                         +

                        Quiz.COLUMN_LAST_RESULT   + " INTEGER, "                      +
                        Quiz.COLUMN_LAST_COMPLETE_RESULT   + " INTEGER, "                      +
                        Quiz.COLUMN_PERCENTAGE   + " INTEGER, "                       +
                        Quiz.COLUMN_LAST_ANSWERED_QUESTION_ID   + " INTEGER, " +
                        Quiz.COLUMN_DONE                        +   " INTEGER, "      +
                        Quiz.COLUMN_QUESTION_COUNT              +   " INTEGER "     +   ");";


        final String SQL_CREATE_QUESTIONS_TABLE =
                "CREATE TABLE " + Questions.TABLE_NAME+ " (" +
                        Questions.COLUMN_QUESTION_ID    + " INTEGER PRIMARY KEY AUTOINCREMENT, "    +
                        Questions.COLUMN_QUIZ_ID        + " INTEGER, "                +
                        Questions.COLUMN_QUESTION_TEXT  + " TEXT, "                   +
                        Questions.COLUMN_IF_ANSWERED      + " INTEGER, "                   +
                        Questions.COLUMN_ANSWERED_CORRECT        + " INTEGER "                +");";


        final String SQL_CREATE_ANSWERS_TABLE =
                "CREATE TABLE " + Answers.TABLE_NAME     + " ("                       +

                        Answers.COLUMN_ANSWER_ID        + " INTEGER PRIMARY KEY AUTOINCREMENT, "    +
                        Answers.COLUMN_QUESTION_ID      + " INTEGER, "                 +
                        Answers.COLUMN_QUIZ_ID          + " INTEGER, "                 +
                        Answers.COLUMN_ANSWER_TEXT      + " TEXT, "                    +
                        Answers.COLUMN_IS_CORRECT       + " INTEGER "                    + ");";


        sqLiteDatabase.execSQL(SQL_CREATE_QUIZ_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_QUESTIONS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_ANSWERS_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Quiz.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Questions.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Answers.TABLE_NAME);



        onCreate(sqLiteDatabase);
    }

    public boolean isEmpty(String tableName){
        SQLiteDatabase db = this.getReadableDatabase();
        String count = "SELECT count(*) FROM "+tableName;
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if(icount>0)
            return false;
        else
            return true;


    }

    public QuizDBHelper getmDBHelper (){
        return this;
    }

    public void insertQuizList(QuizList quizList){
        for (Item item: quizList.getItems()  ) {

            insertQuiz(item.getTitle(),item.getId(),item.getMainPhoto().getUrl(), item.getQuestions());
        }
    }

    public void insertQuiz(String title, long id, String photo, int count){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(QuizContract.Quiz.COLUMN_QUIZ_TITLE, title);
        values.put(QuizContract.Quiz.COLUMN_QUIZ_ID,id);
        values.put(QuizContract.Quiz.COLUMN_QUIZ_PHOTO, photo);
        values.put(QuizContract.Quiz.COLUMN_QUESTION_COUNT, count);
        values.put(QuizContract.Quiz.COLUMN_PERCENTAGE,0);
        values.put(QuizContract.Quiz.COLUMN_LAST_COMPLETE_RESULT,0);
        values.put(Quiz.COLUMN_LAST_ANSWERED_QUESTION_ID,0);
        values.put(QuizContract.Quiz.COLUMN_DONE,0);


        long newRowId = db.insert(QuizContract.Quiz.TABLE_NAME, null, values);

        //db.close();

    }

    public long insertQuestion(long quizID, String text ){
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        // values.put(QuizContract.Questions.COLUMN_QUESTION_ID, questionID);
        values.put(QuizContract.Questions.COLUMN_QUIZ_ID, quizID);
        values.put(QuizContract.Questions.COLUMN_QUESTION_TEXT, text);
        values.put(QuizContract.Questions.COLUMN_IF_ANSWERED, 0);

       long newRowId = db.insert(QuizContract.Questions.TABLE_NAME, null, values);

        db.close();
        return newRowId;
    }

    public void insertAnswer( long quizID, long questionID, String answerText, int isCorrect ){
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        //values.put(QuizContract.Answers.COLUMN_ANSWER_ID, answerID);
        values.put(QuizContract.Answers.COLUMN_QUIZ_ID, quizID);
        values.put(QuizContract.Answers.COLUMN_QUESTION_ID,questionID);
        values.put(QuizContract.Answers.COLUMN_ANSWER_TEXT, answerText);
        values.put(QuizContract.Answers.COLUMN_IS_CORRECT, isCorrect);

        long newRowId = db.insert(QuizContract.Answers.TABLE_NAME, null, values);
        db.close();

    }


    public int updateQuizLastScore(long quizID, double lastScore){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(QuizContract.Quiz.COLUMN_LAST_RESULT,lastScore);

        String selection = QuizContract.Quiz.COLUMN_QUIZ_ID + " = " + quizID;
        //String[] selectionArgs = { "MyOldTitle" };

        int count = db.update(
                QuizContract.Quiz.TABLE_NAME,
                values,
                selection,
                null);

        db.close();
        return count;
    }
    public int updateQuizLastCompleteScore(long quizID, int lastScore){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(QuizContract.Quiz.COLUMN_LAST_COMPLETE_RESULT,lastScore);

        String selection = QuizContract.Quiz.COLUMN_QUIZ_ID + " = " + quizID;
        //String[] selectionArgs = { "MyOldTitle" };

        int count = db.update(
                QuizContract.Quiz.TABLE_NAME,
                values,
                selection,
                null);

        db.close();
        return count;
    }

    public int updateQuizDone(long quizID, int done){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(QuizContract.Quiz.COLUMN_DONE,done);

        String selection = QuizContract.Quiz.COLUMN_QUIZ_ID + " = " + quizID;
        //String[] selectionArgs = { "MyOldTitle" };

        int count = db.update(
                QuizContract.Quiz.TABLE_NAME,
                values,
                selection,
                null);

        db.close();
        return count;
    }

    public int updateQuizPercentage(long quizID, double percentage){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(QuizContract.Quiz.COLUMN_PERCENTAGE,percentage);

        String selection = QuizContract.Quiz.COLUMN_QUIZ_ID + " = " + quizID;


        int count = db.update(
                QuizContract.Quiz.TABLE_NAME,
                values,
                selection,
                null);

        db.close();
        return count;
    }

    public int updateQuestionIsAnswered(long questionId, int answered){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(QuizContract.Questions.COLUMN_IF_ANSWERED, answered);

        String selection = QuizContract.Questions.COLUMN_QUESTION_ID + " = " + questionId;


        int count = db.update(
                QuizContract.Questions.TABLE_NAME,
                values,
                selection,
                null);

        db.close();
        return count;
    }

    public int updateAnsweredCorrect(long questionId, int correct){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(QuizContract.Questions.COLUMN_ANSWERED_CORRECT, correct);

        String selection = QuizContract.Questions.COLUMN_QUESTION_ID + " = " + questionId;


        int count = db.update(
                QuizContract.Questions.TABLE_NAME,
                values,
                selection,
                null);

        db.close();
        return count;
    }


    public int getIfAnsweredQuestion(long questionId){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String[] projection = {QuizContract.Questions.COLUMN_IF_ANSWERED};
        cursor = db.query(QuizContract.Questions.TABLE_NAME, projection, QuizContract.Questions.COLUMN_QUESTION_ID+" = "+ questionId, null,null,null,null);

        if(cursor.moveToFirst()) {
            db.close();
            return cursor.getInt(cursor.getColumnIndexOrThrow(QuizContract.Questions.COLUMN_IF_ANSWERED));
        }
        else{
            db.close();
            return -1;
        }

    }

    public int getPercentage(long quizId){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String[] projection = {Quiz.COLUMN_PERCENTAGE};
        cursor = db.query(QuizContract.Quiz.TABLE_NAME, projection, QuizContract.Quiz.COLUMN_QUIZ_ID+" = "+ quizId, null,null,null,null);

        if(cursor.moveToFirst()) {
            db.close();
            return cursor.getInt(cursor.getColumnIndexOrThrow(Quiz.COLUMN_PERCENTAGE));

        }
        else {
            db.close();
            return -1;
        }
    }

    public int getIfCorrectAnsweredQuestion(long questionId){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String[] projection = {Questions.COLUMN_ANSWERED_CORRECT};
        cursor = db.query(QuizContract.Questions.TABLE_NAME, projection, QuizContract.Questions.COLUMN_QUESTION_ID+" = "+ questionId, null,null,null,null);

        if(cursor.moveToFirst()) {
            db.close();
            return cursor.getInt(cursor.getColumnIndexOrThrow(Questions.COLUMN_ANSWERED_CORRECT));
        }else {
            db.close();
            return -1;
        }
    }

    public int getLastScore(long quizId){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String[] projection = {QuizContract.Quiz.COLUMN_LAST_RESULT};
        cursor = db.query(QuizContract.Quiz.TABLE_NAME, projection, Quiz.COLUMN_QUIZ_ID+" = "+ quizId, null,null,null,null);

        if(cursor.moveToFirst()) {
            db.close();

            return cursor.getInt(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_LAST_RESULT));
        }else {
            db.close();
            return -1;
        }
    }

    public int getLastCompleteScore(long quizId){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String[] projection = {QuizContract.Quiz.COLUMN_LAST_COMPLETE_RESULT};
        cursor = db.query(QuizContract.Quiz.TABLE_NAME, projection, Quiz.COLUMN_QUIZ_ID+" = "+ quizId, null,null,null,null);

        if(cursor.moveToFirst()) {
            db.close();
            return cursor.getInt(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_LAST_COMPLETE_RESULT));

        }else {
            db.close();
            return -1;
        }
    }

    public int getIfDone(long quizId){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        String[] projection = {Quiz.COLUMN_DONE};
        cursor = db.query(QuizContract.Quiz.TABLE_NAME, projection, Quiz.COLUMN_QUIZ_ID+" = "+ quizId, null,null,null,null);

        if(cursor.moveToFirst()) {
            db.close();
            return cursor.getInt(cursor.getColumnIndexOrThrow(Quiz.COLUMN_DONE));
        }
        else {
            db.close();

            return -1;
        }
    }
    public QuizList getQuizList(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        List<Item> itemsList = new ArrayList<Item>();
        QuizList quizList = new QuizList();



        cursor = db.query(QuizContract.Quiz.TABLE_NAME,null,null,null,null,null,null);

        while(cursor.moveToNext()) {
            Item item = new Item();
            item.setId(cursor.getLong(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_QUIZ_ID)));
            MainPhoto quizPhoto = new MainPhoto();
            quizPhoto.setSource(cursor.getString(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_QUIZ_PHOTO)));
            item.setMainPhoto(quizPhoto);
            item.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_QUIZ_TITLE)));
            item.setLastScore(cursor.getInt(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_LAST_RESULT)));
            item.setLastCompleteResult(cursor.getInt(cursor.getColumnIndexOrThrow(Quiz.COLUMN_LAST_COMPLETE_RESULT)));
            item.setPercentage(cursor.getInt(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_PERCENTAGE)));
            item.setQuestions(cursor.getInt(cursor.getColumnIndexOrThrow(Quiz.COLUMN_QUESTION_COUNT)));
            item.setLastAnsweredQuestionID(cursor.getInt(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_LAST_ANSWERED_QUESTION_ID)));


            itemsList.add(item);

        }
        quizList.setItems(itemsList);
        cursor.close();

        db.close();
        return quizList;
    }

    public pl.o2.quiz.mobile.gson.Quiz getQuiz(long quizId){
        pl.o2.quiz.mobile.gson.Quiz quiz = new pl.o2.quiz.mobile.gson.Quiz();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor, cursorQuestions, cursorAnswer;
        int questionID;
        List<Question> questions = new ArrayList<Question>();

        String  selection = QuizContract.Quiz.COLUMN_QUIZ_ID+" = ? ";
        String [] selectionArgs = {String.valueOf(quizId)};

        String [] projectionQuiz = {QuizContract.Quiz.COLUMN_QUIZ_ID, QuizContract.Quiz.COLUMN_QUIZ_TITLE, QuizContract.Quiz.COLUMN_PERCENTAGE, QuizContract.Quiz.COLUMN_LAST_ANSWERED_QUESTION_ID};

        String [] projectionQuestion = {QuizContract.Questions.COLUMN_QUESTION_ID, QuizContract.Questions.COLUMN_QUESTION_TEXT, QuizContract.Questions.COLUMN_IF_ANSWERED, Questions.COLUMN_ANSWERED_CORRECT};

        String [] projectionAnswers = {QuizContract.Answers.COLUMN_ANSWER_ID,QuizContract.Answers.COLUMN_ANSWER_TEXT,QuizContract.Answers.COLUMN_IS_CORRECT};

        cursor = db.query(QuizContract.Quiz.TABLE_NAME,projectionQuiz, selection,selectionArgs,null,null,null);
        cursor.moveToFirst();

        quiz.setId(cursor.getLong(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_QUIZ_ID)));
        quiz.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_QUIZ_TITLE)));
        quiz.setPercentage(cursor.getInt(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_PERCENTAGE)));
        quiz.setLastAnsweredQuestionID(cursor.getInt(cursor.getColumnIndexOrThrow(QuizContract.Quiz.COLUMN_LAST_ANSWERED_QUESTION_ID)));
        ;

        cursorQuestions = db.query(QuizContract.Questions.TABLE_NAME, projectionQuestion, QuizContract.Questions.COLUMN_QUIZ_ID+" = "+quizId, null,null,null,null);

        while(cursorQuestions.moveToNext()) {

            Question question = new Question();
            List<Answer> answers = new ArrayList<Answer>();
            question.setId(cursorQuestions.getLong(cursorQuestions.getColumnIndexOrThrow(QuizContract.Questions.COLUMN_QUESTION_ID)));
            question.setText(cursorQuestions.getString(cursorQuestions.getColumnIndexOrThrow(QuizContract.Questions.COLUMN_QUESTION_TEXT)));
            question.setIsAnswered(cursorQuestions.getInt(cursorQuestions.getColumnIndexOrThrow(Questions.COLUMN_IF_ANSWERED)));
            question.setIsAnsweredCorrect(cursorQuestions.getInt(cursorQuestions.getColumnIndexOrThrow(Questions.COLUMN_ANSWERED_CORRECT)));
            cursorAnswer = db.query(QuizContract.Answers.TABLE_NAME, projectionAnswers, QuizContract.Answers.COLUMN_QUESTION_ID + " = " + question.getId(), null, null, null, null);
            while(cursorAnswer.moveToNext()){
                Answer answer = new Answer();
                answer.setId(cursorAnswer.getLong(cursorAnswer.getColumnIndexOrThrow(QuizContract.Answers.COLUMN_ANSWER_ID)));
                answer.setText(cursorAnswer.getString(cursorAnswer.getColumnIndexOrThrow(QuizContract.Answers.COLUMN_ANSWER_TEXT)));
                answer.setIsCorrect(cursorAnswer.getInt(cursorAnswer.getColumnIndexOrThrow(QuizContract.Answers.COLUMN_IS_CORRECT)));
                answers.add(answer);
            }
            cursorAnswer.close();

            question.setAnswers(answers);
            questions.add(question);
        }

        quiz.setQuestions(questions);
        cursor.close();
        cursorQuestions.close();
        db.close();
        return quiz;
    }


    public int updateLastAnsweredQuestionID(long quizID, long lastQuestionId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Quiz.COLUMN_LAST_ANSWERED_QUESTION_ID,lastQuestionId);

        String selection = QuizContract.Quiz.COLUMN_QUIZ_ID + " = " + quizID;
        //String[] selectionArgs = { "MyOldTitle" };

        int count = db.update(
                QuizContract.Quiz.TABLE_NAME,
                values,
                selection,
                null);

        db.close();
        return count;
    }
}




