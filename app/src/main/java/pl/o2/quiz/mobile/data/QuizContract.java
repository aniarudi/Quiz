package pl.o2.quiz.mobile.data;

/**
 * Created by rudi on 07.04.18.
 */
import android.provider.BaseColumns;

public class QuizContract {


    private QuizContract(){

    }

    public static final class Quiz implements BaseColumns{


        public static final String TABLE_NAME = "quiz";

        public static final String COLUMN_QUIZ_ID = "quizID";

        public static final String COLUMN_QUIZ_TITLE = "quizTitle";

        public static final String COLUMN_QUIZ_PHOTO = "quizPhoto";

        public static final String COLUMN_LAST_RESULT = "lastResult";

        public static final String COLUMN_LAST_COMPLETE_RESULT = "lastCompleteResult";

        public static final String COLUMN_PERCENTAGE = "percentage";

        public static final String COLUMN_LAST_ANSWERED_QUESTION_ID = "lastAnsweredQuestionID";

        public static final String COLUMN_DONE = "done";

        public static final String COLUMN_QUESTION_COUNT = "questioncount";

    }



    public static final class Questions implements BaseColumns {
        public static final String TABLE_NAME = "questions";

        public static final String COLUMN_QUIZ_ID = "quizID";

        public static final String COLUMN_QUESTION_ID = "questionID";

        public static final String COLUMN_QUESTION_TEXT = "questionText";
        public static final String COLUMN_IF_ANSWERED = "ifAnswered";
        public static final String COLUMN_ANSWERED_CORRECT = "answeredCorrect";


    }

    public static final class Answers implements BaseColumns{
        public static final String TABLE_NAME = "answers";

        public static final String COLUMN_ANSWER_ID = "answerId";

        public static final String COLUMN_QUESTION_ID = "questionID";

        public static final String COLUMN_QUIZ_ID = "quizID";

        public static final String COLUMN_ANSWER_TEXT = "answerText";

        public static final String COLUMN_IS_CORRECT = "isCorrect";



    }




}
