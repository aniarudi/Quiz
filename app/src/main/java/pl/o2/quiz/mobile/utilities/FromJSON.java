package pl.o2.quiz.mobile.utilities;

/**
 * Created by rudi on 08.04.18.
 */

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import pl.o2.quiz.mobile.gson.Quiz;
import pl.o2.quiz.mobile.gson.QuizList;


public class FromJSON {

    public static QuizList getQuizList(String json){
        Gson gson = new Gson();
        //Type type = new TypeToken<QuizList>() {}.getType();
        QuizList fromJson = gson.fromJson(json, QuizList.class);
        return fromJson;
    }

    public static Quiz getQuiz(String json){
        Gson gson = new Gson();
        Type type = new TypeToken<Quiz>() {}.getType();
        Quiz fromJson = gson.fromJson(json,type);
        return fromJson;
    }
}
