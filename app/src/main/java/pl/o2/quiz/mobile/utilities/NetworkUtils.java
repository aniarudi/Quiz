package pl.o2.quiz.mobile.utilities;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by rudi on 08.04.18.
 */

public class NetworkUtils {


    private static final String QUIZ_LIST_URL = "http://quiz.o2.pl/api/v1/quizzes/0/100";

    private static final String BASE_QUIZ_URL = "http://quiz.o2.pl/api/v1/quiz/";



    public static URL buildQuizListUrl() {

        try {
            URL quizListUrl = new URL(QUIZ_LIST_URL);

            return quizListUrl;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static URL buildQuizUrl(long quizID) {

        try {
            URL quizListUrl = new URL(BASE_QUIZ_URL+quizID+"/0");

            return quizListUrl;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);


            //do przetestowania!!!
            scanner.useDelimiter("\\A");  //"}}]}"

            boolean hasInput = scanner.hasNext();
            String response = null;
            if (hasInput) {
                response = scanner.next();
            }
            scanner.close();
            return response;
        } finally {
            urlConnection.disconnect();
        }
    }
}
