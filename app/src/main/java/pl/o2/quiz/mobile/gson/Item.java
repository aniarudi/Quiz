
package pl.o2.quiz.mobile.gson;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("buttonStart")
    @Expose
    private String buttonStart;
    @SerializedName("shareTitle")
    @Expose
    private String shareTitle;
    @SerializedName("questions")
    @Expose
    private int questions;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("sponsored")
    @Expose
    private boolean sponsored;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("mainPhoto")
    @Expose
    private MainPhoto mainPhoto;
    @SerializedName("category")
    @Expose
    private Category_ category;

    private int done;


    private int lastScore;
    private int lastCompleteResult;

    private int percentage;

    private int lastAnsweredQuestionID;

    public int getDone(){return done;}

    public void setDone(int d){this.done = d;}

    public String getButtonStart() {
        return buttonStart;
    }

    public void setButtonStart(String buttonStart) {
        this.buttonStart = buttonStart;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public int getQuestions() {
        return questions;
    }

    public void setQuestions(int questions) {
        this.questions = questions;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isSponsored() {
        return sponsored;
    }

    public void setSponsored(boolean sponsored) {
        this.sponsored = sponsored;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MainPhoto getMainPhoto() {
        return mainPhoto;
    }

    public void setMainPhoto(MainPhoto mainPhoto) {
        this.mainPhoto = mainPhoto;
    }

    public Category_ getCategory() {
        return category;
    }

    public void setCategory(Category_ category) {
        this.category = category;
    }

    public void setLastScore(int lastScore) { this.lastScore = lastScore;}

    public void setPercentage(int percentage) {this.percentage = percentage;}

    public void setLastAnsweredQuestionID(int lastAnsweredQuestionID){this.lastAnsweredQuestionID = lastAnsweredQuestionID;}

    public int getLastScore() { return this.lastScore ;}

    public int getPercentage() {return this.percentage;}

    public int getLastAnsweredQuestionID(){return this.lastAnsweredQuestionID ;}

    public int getLastCompleteResult() {
        return lastCompleteResult;
    }

    public void setLastCompleteResult(int lastCompleteResult) {
        this.lastCompleteResult = lastCompleteResult;
    }
}
