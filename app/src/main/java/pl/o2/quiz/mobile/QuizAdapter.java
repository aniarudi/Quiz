package pl.o2.quiz.mobile;

/**
 * Created by rudi on 10.04.18.
 */


import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import pl.o2.quiz.mobile.app.R;

import pl.o2.quiz.mobile.data.QuizDBHelper;
import pl.o2.quiz.mobile.gson.QuizList;

public class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.QuizAdapterViewHolder> {

    private QuizList quizListItems;
    private Context context;
    private RecyclerView recyclerView;
    private QuizDBHelper quizDBHelper;

    private QuizAdapterOnClickHandler mClickHandler;

    public interface QuizAdapterOnClickHandler {
        void onClick(View view, int position);
    }

    public QuizAdapter(QuizList quizList, Context c, RecyclerView rv,QuizAdapterOnClickHandler clickHandler) {
        quizListItems = quizList;
        context = c;
        recyclerView = rv;
        mClickHandler = clickHandler;
        quizDBHelper = new QuizDBHelper(context);
    }


    public class QuizAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView quizCardView;
        TextView quizTitle;
        ImageView quizImage;
        TextView quizLastScore;
        TextView quizPercentage;

        public QuizAdapterViewHolder(View itemView, QuizAdapterOnClickHandler listener) {
            super(itemView);
            quizCardView = (CardView) itemView.findViewById(R.id.quizlist_cardview);
            quizTitle = (TextView) itemView.findViewById(R.id.quiz_title);
            quizImage = (ImageView) itemView.findViewById(R.id.quiz_photo);
            quizLastScore = (TextView) itemView.findViewById(R.id.quiz_last_score);
            quizPercentage = (TextView) itemView.findViewById(R.id.quiz_percentage);
            mClickHandler = listener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            mClickHandler.onClick(view, getAdapterPosition());
        }
    }


        @Override
    public QuizAdapter.QuizAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            Context context = parent.getContext();
            int layoutIdForListItem = R.layout.activity_quist_list_item;
            LayoutInflater inflater = LayoutInflater.from(context);
            boolean shouldAttachToParentImmediately = false;

            View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
            return new QuizAdapterViewHolder(view,mClickHandler);

    }

    @Override
    public void onBindViewHolder(QuizAdapter.QuizAdapterViewHolder quizAdapterViewHolder, int position) {

        System.out.println("onBind poszedł");

        quizAdapterViewHolder.quizTitle.setText(quizListItems.getItem(position).getTitle());
       // TODO Rozwiązać uzyskiwanie obrakza quizAdapterViewHolder.quizImage.setImageURI( quizListItems.getItem(position).getMainPhoto().);
        Picasso.get().load(quizListItems.getItem(position).getMainPhoto().getSource()).into(quizAdapterViewHolder.quizImage);
        int lastScore, questionsCount, percentage;
        if(quizDBHelper.getIfDone(quizListItems.getItem(position).getId()) == 1){

            lastScore = quizListItems.getItem(position).getLastCompleteResult();
            Log.d("last score", ""+lastScore);
            questionsCount = quizListItems.getItem(position).getQuestions();
            Log.d("lquestion count", ""+questionsCount);
            //String lastScoreString = String.format(res.getString(R.string.last_result),lastScore));
            quizAdapterViewHolder.quizLastScore.setText("Ostatni wynik: "+lastScore+"/"+questionsCount);
            //quizAdapterViewHolder.quizLastScore.setVisibility(View.VISIBLE);

        }
            percentage = quizListItems.getItem(position).getPercentage();
            quizAdapterViewHolder.quizPercentage.setText(""+percentage+"%");

    }

    @Override
    public int getItemCount() {
        if(quizListItems==null)
            return 0;
        return quizListItems.getItems().size();
    }

    public void setQuizesData(QuizList quizList) {
        quizListItems = quizList;
        notifyDataSetChanged();
    }

    public QuizList getQuizListItems(){
        return quizListItems;
    }
}
