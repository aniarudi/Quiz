
package pl.o2.quiz.mobile.gson;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuizList {

    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Item> getItems() {
        return items;
    }

    public Item getItem(int position) {return items.get(position);}

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void addItemToList(Item item) {this.items.add(item);}

}
