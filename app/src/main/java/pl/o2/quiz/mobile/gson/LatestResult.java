
package pl.o2.quiz.mobile.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LatestResult {

    @SerializedName("city")
    @Expose
    private int city;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("result")
    @Expose
    private double result;
    @SerializedName("resolveTime")
    @Expose
    private int resolveTime;

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public int getResolveTime() {
        return resolveTime;
    }

    public void setResolveTime(int resolveTime) {
        this.resolveTime = resolveTime;
    }

}
