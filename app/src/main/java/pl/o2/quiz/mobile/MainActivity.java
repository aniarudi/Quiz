package pl.o2.quiz.mobile;

import pl.o2.quiz.mobile.data.QuizContract;
import pl.o2.quiz.mobile.data.QuizDBHelper;
import pl.o2.quiz.mobile.gson.QuizList;
import pl.o2.quiz.mobile.utilities.*;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.net.URL;

import pl.o2.quiz.mobile.app.R;
import pl.o2.quiz.mobile.gson.*;

public class MainActivity extends AppCompatActivity implements QuizAdapter.QuizAdapterOnClickHandler, LoaderCallbacks<QuizList> {

    CardView cv;

    private static final String TAG = MainActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mlayoutManager;
    private QuizDBHelper mdbHelper;
    private QuizAdapter mQuizAdapter;
    private QuizList quizList;

    private static final int QUIZ_LOADER_ID = 0;
    private ProgressBar progressBar;
    private TextView progressBarText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_quiz);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBarText = (TextView) findViewById(R.id.progressBarText);


        mlayoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(mlayoutManager);

        mRecyclerView.setHasFixedSize(true);

        mdbHelper = new QuizDBHelper(this);

        if(!mdbHelper.isEmpty(QuizContract.Quiz.TABLE_NAME)) {
            quizList = mdbHelper.getQuizList();
        }
        mQuizAdapter = new QuizAdapter(quizList,this,mRecyclerView,this);

        mRecyclerView.setAdapter(mQuizAdapter);



        int loaderId = QUIZ_LOADER_ID;

        LoaderCallbacks<QuizList> callback = MainActivity.this;

        Bundle bundleForLoader = null;

        getSupportLoaderManager().initLoader(loaderId, bundleForLoader, callback);




    }


    @Override
    public Loader<QuizList> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<QuizList>(this) {


            QuizList mQuizesData = quizList;


            @Override
            protected void onStartLoading() {
                System.out.println("Async ruszyl");
                if (mQuizesData != null) {
                    deliverResult(mQuizesData);
                    System.out.println("mQuizes not null");

                } else{
                    forceLoad();
                }
            }


            @Override
            public QuizList loadInBackground() {



                if(mdbHelper.isEmpty(QuizContract.Quiz.TABLE_NAME)){
                    URL quizesRequestUrl = NetworkUtils.buildQuizListUrl();


                    try {
                        String jsonQuizListResponse = NetworkUtils
                                .getResponseFromHttpUrl(quizesRequestUrl);

                        quizList = FromJSON.getQuizList(jsonQuizListResponse);

                        android.util.Log.d("lastscoreFromJSON", ""+quizList.getItem(0).getLastScore());

                            mdbHelper.insertQuizList(quizList);
                        System.out.println("                            JSON");

                        Quiz quiz;
                        long questionId;

                        for(Item item: quizList.getItems()){
                            URL quizRequestUrl = NetworkUtils.buildQuizUrl(item.getId());
                           String jsonQuizResponse = NetworkUtils.getResponseFromHttpUrl(quizRequestUrl);
                           quiz = FromJSON.getQuiz(jsonQuizResponse);
                           for(Question question: quiz.getQuestions()) {
                               questionId = mdbHelper.insertQuestion(quiz.getId(), question.getText());
                               for(Answer answer: question.getAnswers()){
                                   mdbHelper.insertAnswer(quiz.getId(),questionId,answer.getText(),answer.getIsCorrectInt());
                               }
                           }
                        }


                        return quizList;
                    } catch (Exception e) {

                        System.out.println("Exception prom asynctask");
                        e.printStackTrace();
                        return null;
                    }
                } else {
                    quizList = mdbHelper.getQuizList();

                   return quizList;
                }



            }


            public void deliverResult(QuizList data) {
                mQuizesData = data;
                super.deliverResult(data);
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<QuizList> loader, QuizList data) {
        progressBar.setVisibility(View.GONE);
        progressBarText.setVisibility(View.GONE);
        if(data == null)
            System.out.println("Dane sie nie zaladowaly");
        else
            System.out.println("Dane sie zaladowaly");
        mQuizAdapter.setQuizesData(data);
        if (null == data) {
            showErrorMessage();
        } else {
            showQuizesDataView();
        }

    }

    @Override
    public void onLoaderReset(Loader<QuizList> loader) {

    }

    @Override
    public void onClick(View view, int position) {

        Context context = this;
        Class destinationClass = QuizActivity.class;
        Intent intentToStartQuizActivity = new Intent(context, destinationClass);

        intentToStartQuizActivity.putExtra("QUIZ_ID", this.mQuizAdapter.getQuizListItems().getItem(position).getId()); //sends clicked quizes id to new activity
        startActivity(intentToStartQuizActivity);
    }



    private void showQuizesDataView() {

        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage() {
        /* First, hide the currently visible data */
        mRecyclerView.setVisibility(View.INVISIBLE);
        /* Then, show the error */
        Toast.makeText(this,"Nie ma danych",Toast.LENGTH_LONG);

    }
}
