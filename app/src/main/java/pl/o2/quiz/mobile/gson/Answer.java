
package pl.o2.quiz.mobile.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Answer {

    @SerializedName("image")
    @Expose
    private Image_ image;
    @SerializedName("order")
    @Expose
    private int order;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("isCorrect")
    @Expose
    private int isCorrect;

    private long id;

    public void setId(long id){this.id = id;}

    public long getId(){return this.id;}

    public Image_ getImage() {
        return image;
    }

    public void setImage(Image_ image) {
        this.image = image;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getIsCorrect() {
        return true ? isCorrect == 1 : false;
    }

    public int getIsCorrectInt(){
        return isCorrect;
    }
    public void setIsCorrect(int isCorrect) {
        this.isCorrect = isCorrect;
    }

}
