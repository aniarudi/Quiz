
package pl.o2.quiz.mobile.gson;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Question {

    @SerializedName("image")
    @Expose
    private Image image;
    @SerializedName("answers")
    @Expose
    private List<Answer> answers = null;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("order")
    @Expose
    private int order;

    private long id;

    private int isAnswered;

    private int isAnsweredCorrect;

    public void setIsAnsweredCorrect(int correct){isAnsweredCorrect = correct;}

    public boolean getIsAnsweredCorrect(){return true ? isAnsweredCorrect == 1 : false;}

    public void setIsAnswered(int is) { isAnswered = is;}

    public boolean getIsAnswered(){return true ? isAnswered>0 : false;}

    public void setId(long id) {this.id = id;}

    public long getId(){return this.id;}

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

}
