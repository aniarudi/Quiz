package pl.o2.quiz.mobile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import pl.o2.quiz.mobile.app.R;

import pl.o2.quiz.mobile.data.QuizDBHelper;
import pl.o2.quiz.mobile.gson.*;
import pl.o2.quiz.mobile.*;

/**
 * Created by rudi on 11.04.18.
 */

public class QuizActivity extends AppCompatActivity {


    private TextView qTitle, qQuestion, goodAnsweredQuestions;
    private RadioButton qAnswer1, qAnswer2, qAnswer3, qAnswer4;
    private RadioGroup answersRG, radioGroup;
    private ProgressBar qProgress;
    private int questionsCount, questionCursor, goodAnswers;
    private LinearLayout llQuizEnd, quizView;


    private RadioButton [] answers;
    private long quizId;


    QuizDBHelper mDBHelper;
    Quiz quiz;
    Question question;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_quiz);



        qTitle = (TextView) findViewById(R.id.one_quiz_title);
        goodAnsweredQuestions = (TextView) findViewById(R.id.percent);
        qProgress = (ProgressBar) findViewById(R.id.quiz_progress);
        qQuestion = (TextView) findViewById(R.id.question);

        qAnswer1 = (RadioButton) findViewById(R.id.answer1);
        qAnswer2 = (RadioButton) findViewById(R.id.answer2);
        qAnswer3 = (RadioButton) findViewById(R.id.answer3);
        qAnswer4 = (RadioButton) findViewById(R.id.answer4);
        answersRG = (RadioGroup) findViewById(R.id.answers_radio_group);

        llQuizEnd = (LinearLayout) findViewById(R.id.quiz_end);
        quizView = (LinearLayout) findViewById(R.id.quiz_view);

        answers = new RadioButton[] {qAnswer1,qAnswer2,qAnswer3,qAnswer4};

        mDBHelper = new QuizDBHelper(this);

        goodAnswers =0;

        Intent intentThatStartedThisActivity = getIntent();

        if (intentThatStartedThisActivity != null) {
            if (intentThatStartedThisActivity.hasExtra("QUIZ_ID")) {
                quizId = intentThatStartedThisActivity.getLongExtra("QUIZ_ID", 1);

                Log.d("quizId", "quizID = "+quizId);
                quiz = mDBHelper.getQuiz(quizId);

                questionsCount = quiz.getQuestions().size();
                qProgress.setMax(questionsCount);
                questionCursor=0;

                if(quiz.getLastAnsweredQuestionID() == 0){
                    for(Question question:quiz.getQuestions()) {
                        mDBHelper.updateQuestionIsAnswered(question.getId(),0);

                    }
                } else {
                    while (quiz.getQuestions().get(questionCursor).getIsAnswered()) {
                        if(quiz.getQuestions().get(questionCursor).getIsAnsweredCorrect())
                            goodAnswers++;
                        questionCursor++;
                    }
                }
                qProgress.setProgress(questionCursor);
                qProgress.setVisibility(View.VISIBLE);
                qTitle.setText(quiz.getTitle());
                qQuestion.setText(quiz.getQuestions().get(questionCursor).getText());
                for (int i = 0; i < quiz.getQuestions().get(questionCursor).getAnswers().size() && i < 4; i++) {
                    answers[i].setText(quiz.getQuestions().get(questionCursor).getAnswers().get(i).getText());
                    answers[i].setVisibility(View.VISIBLE);

                }

            }

            answersRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                    int index;
                    mDBHelper.updateLastAnsweredQuestionID(quiz.getId(), quiz.getQuestions().get(questionCursor).getId());
                    mDBHelper.updateQuizPercentage(quiz.getId(),Math.round((questionCursor+1)*100f/questionsCount));
                    mDBHelper.updateQuestionIsAnswered(quiz.getQuestions().get(questionCursor).getId(),1);
                    View radioButton = answersRG.findViewById(checkedId);
                    index = answersRG.indexOfChild(radioButton);


                    if(quiz.getQuestions().get(questionCursor).getAnswers().get(index).getIsCorrect()){
                        goodAnswers++;
                        Log.d("goodAnswers", "good answers ="+goodAnswers);
                        mDBHelper.updateQuizLastScore(quiz.getId(),goodAnswers);
                        mDBHelper.updateAnsweredCorrect(quiz.getQuestions().get(questionCursor).getId(),1);

                    } else{
                        mDBHelper.updateAnsweredCorrect(quiz.getQuestions().get(questionCursor).getId(),0);

                    }
                    if(questionCursor == questionsCount-1){

                        mDBHelper.updateQuizDone(quiz.getId(),1);
                        mDBHelper.updateQuizLastCompleteScore(quiz.getId(),goodAnswers);
                        mDBHelper.updateLastAnsweredQuestionID(quiz.getId(), 0);

                        quizView.setVisibility(View.INVISIBLE);
                        Log.d("allQuestions", "w quizie jest"+questionsCount);
                        Log.d("percent", "procentowo"+((double)goodAnswers/questionsCount));
                        goodAnsweredQuestions.setText((Math.round((double)(goodAnswers*100/questionsCount)))+"%");
                        llQuizEnd.setVisibility(View.VISIBLE);

                    } else {
                        questionCursor++;
                        qProgress.setProgress(questionCursor);
                        clearAnswersRadioButtonVisibility();

                        qQuestion.setText(quiz.getQuestions().get(questionCursor).getText());
                        for (int i = 0; i < quiz.getQuestions().get(questionCursor).getAnswers().size() && i < 4; i++) {
                            answers[i].setText(quiz.getQuestions().get(questionCursor).getAnswers().get(i).getText());
                            answers[i].setVisibility(View.VISIBLE);

                        }
                    }


                    Log.d("indeks", "="+index);

                }
            });

        }


    }





    private void clearAnswersRadioButtonVisibility(){
        for(RadioButton rb: answers) {
            rb.setVisibility(View.INVISIBLE);
            rb.setChecked(false);
        }

    }

    public void toQuizList(View view) {
        Context context = this;
        Class destinationClass = MainActivity.class;
        Intent intentToStartQuizActivity = new Intent(context, destinationClass);
        startActivity(intentToStartQuizActivity);
    }

    public void quizOnceAgain(View view) {
        Context context = this;
        Class destinationClass = QuizActivity.class;

        Intent intentToStartQuizActivity = new Intent(context, destinationClass);
        intentToStartQuizActivity.putExtra("QUIZ_ID", quizId); //sends clicked quizes id to new activity

        startActivity(intentToStartQuizActivity);
    }
}
